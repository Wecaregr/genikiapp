import zeep

class genapi:
    wsdl = 'http://voucher.taxydromiki.gr/JobServices.asmx?WSDL'
    key=''
    client = zeep.Client(wsdl=wsdl)
    #####################################################################################################################
    # AuthenticateResult Authenticate(string sUsrName, string sUsrPwd, string applicationKey)
    # User name, password and application key are passed and an object with the result code and an authentication key is
    # returned. This authentication key is used with all other methods.
    ######################################################################################################################
    def Authenticate(self, username, password, apiKey):
        result = self.client.service.Authenticate(username, password, apiKey)
        print('|--',result)
        if result['Result']==0:
            self.key = result['Key']
            return True
        else:
            return False

    ########################################################################################################
    # TrackAndTraceResult TrackAndTrace(string authKey, string voucherNo, string language)
    # Gets a history of checkpoints for the specified voucher number and its current status (delivered/not delivered).
    # Language can be “el” (for Greek) or “en” (for English).
    ##################################################################################################################
    def TrackAndTrace(self, voucherNo, language='el'):
        result = self.client.service.TrackAndTrace(self.key, voucherNo, language)
        #print(result)
        return result

    ########################################################################################################
    # GetShopsListResult GetShopsList(string authKey)
    # Gets a list of Geniki Taxydromiki shops.
    #####################################################################################
    def GetShopsList(self):
        result = self.client.service.GetShopsList(self.key)
        return result


    def __repr__(self):
        return f"genapi('{self.key}', '{self.wsdl}')"

    def __init__(self, usname, password, apiKey):
        result = self.Authenticate(usname, password, apiKey)
        print('|--',result)



#ga = genapi('wecare', '2683023055n', 'E94914A8-B34A-4C7B-A7A6-B2A564D0E920')
#print(ga)
#ga.TrackAndTrace('4014826233')
