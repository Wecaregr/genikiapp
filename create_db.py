from models import db
from gen_main import app

with app.test_request_context():
     db.init_app(app)

     db.create_all()
