import json
import requests

class sms:
    token=''

    extapipath ='https://easysms.gr/api/'
    def balance(self):        
        response = requests.get(self.extapipath+"me/balance?key={0}&type=json".format(self.token))
        try:
            json_data = json.loads(response.text)
            return json_data
        except:
            return {"status":"0","remarks":"Error in mobilecheck"}

    def mobilecheck(self, mobile):
        response = requests.get(self.extapipath+"mobile/check?mobile={0}&type=json".format(mobile))
        try:
            json_data = json.loads(response.text)
            return json_data
        except:
            return {"status":"0","remarks":"Error in mobilecheck"}

    def sendsms(self, txt, fromus, mobile ):
        checkmob = self.mobilecheck(mobile)
        if checkmob['status']=='1':
            response = requests.get(self.extapipath+"sms/send?key={0}&text={1}&from={2}&to={3}&type=json".format(self.token, txt, fromus, mobile ))
            try:
                json_data = json.loads(response.text)
                return json_data
            except:
                return {"status":"0","remarks":"Error in Send sms"}
        else:
            return {"status":"0","remarks":"Mobile not eligible"}

    def RefreshExtApiToken(self, extapi_un, extapi_pass):
        response = requests.get(self.extapipath+'/key/get?username=vlasisgeo@gmail.com&password=74992&type=json'.format(extapi_un, extapi_pass))
        try:
            json_data = json.loads(response.text)
            self.token = json_data['key']
            print ('|--Sms APi Success',json_data)
            return True
        except:
            json_data = response.text
            self.token=None
            print ('|--Error Authendicating Sms APi',json_data)
            return False


    def __init__(self, username, password):
        self.RefreshExtApiToken(username, password)
