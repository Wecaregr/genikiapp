#import urllib.request, urllib.error
import json
import requests

class Extapi:
    token = ""
    #extapipath ='https://wecarepharm.gr:5001/api/v1.0'
    extapipath ='http://192.168.0.148:5001/api/v1.0'
    def callExtApi(self, datajson):
        global token
        headers = {'content-type': 'application/json',
    	'Authorization': 'JWT {0}'.format(self.token)}
        print('|--',datajson)
        response = requests.post(self.extapipath+'/service', data=json.dumps(datajson), headers=headers, verify=False)
        try:
            json_data = json.loads(response.text)
        except:
            json_data = response.text
            self.token = self.RefreshExtApiToken()
            self.callExtApi(datajson)
        return json_data


    def RefreshExtApiToken(self, extapi_un, extapi_pass):
        headers = {'content-type': 'application/json'}
        #extApiResponce = callExtApi(extapipath+'/auth',{"username":extapi_un, "password": extapi_pass} )
        response = requests.post(self.extapipath+'/auth',
                                 data=json.dumps({"username":extapi_un, "password": extapi_pass}),
                                 headers=headers)
        try:
            json_data = json.loads(response.text)
            self.token = json_data['access_token']
            print ('|--Wecare api Authendication Success',json_data)
            return True
        except:
            json_data = response.text
            self.token=None
            print ('|--Error Authendicating wecare api',json_data)
            return False


    def __init__(self, username, password):
        self.RefreshExtApiToken(username, password)
