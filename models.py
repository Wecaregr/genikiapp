from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
from datetime import datetime, date

db = SQLAlchemy()

class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    order_serial = db.Column(db.Integer, unique=True, nullable=False)
    last_check_point_status = db.Column(db.String(100), unique=False, nullable=True)
    date_last_download = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    date_status_change = db.Column(db.DateTime, nullable=True)
    date_invoiced = db.Column(db.DateTime, nullable=True)
    pricing_name = db.Column(db.String(255), unique=False, nullable=True)
    pricing_surname = db.Column(db.String(255), unique=False, nullable=True)
    voucher_no = db.Column(db.String(50), unique=False, nullable=True)
    gtjobcode = db.Column(db.String(50), unique=False, nullable=True)
    courier = db.Column(db.String(50), unique=False, nullable=True)
    status = db.Column(db.String(30), unique=False, nullable=True)
    gtcount = db.Column(db.Integer, unique=False, nullable=False)
    gtflag = db.Column(db.Integer, unique=False, nullable=False)
    shipping_phone = db.Column(db.String(50), unique=False, nullable=True)
    shipping_mobile = db.Column(db.String(50), unique=False, nullable=True)
    payway = db.Column(db.String(50), unique=False, nullable=True)
    admin_comments = db.Column(db.Text,  unique=False, nullable=True)
    courier_comments = db.Column(db.Text,  unique=False, nullable=True)
    customer_comments = db.Column(db.Text,  unique=False, nullable=True)
    is_resolved = db.Column(db.Integer, nullable=False, default=0)
    is_resolved_timestamp = db.Column(db.DateTime, nullable=True)
    posts = db.relationship('Post', backref='author', lazy=True)

    pricing_address =  db.Column(db.String(255), unique=False, nullable=True)
    pricing_city =  db.Column(db.String(255), unique=False, nullable=True)
    pricing_postal =  db.Column(db.String(50), unique=False, nullable=True)
    shipping_address =  db.Column(db.String(255), unique=False, nullable=True)
    shipping_city =  db.Column(db.String(255), unique=False, nullable=True)
    shipping_postal =  db.Column(db.String(50), unique=False, nullable=True)
    total_vat =  db.Column(db.DECIMAL(8,2),  nullable=True)
    customer_id = db.Column(db.Integer, unique=False, nullable=False)

    customer_email = db.Column(db.String(100), unique=False, nullable=True)
    is_insecure = db.Column(db.Integer, unique=False, nullable=True)

    last_checkpoint_city = db.Column(db.String(255), unique=False, nullable=True)
    last_checkpoint_status = db.Column(db.String(255), unique=False, nullable=True)
    last_checkpoint_date = db.Column(db.String(255), unique=False, nullable=True)


    def __repr__(self):
        return f"Order('{self.order_serial}', '{self.status}', '{self.voucher_no}')"

    def __init__(self, order_serial, status, date_invoiced, pricing_name, pricing_surname,
                 shipping_phone, shipping_mobile, voucher_no, gtjobcode, courier,
                 gen_tax_service, gtflag, gtcount, payway, courier_comments, customer_comments,
                 admin_comments, pricing_address,pricing_city,pricing_postal,
                 shipping_address, shipping_city, shipping_postal, total_vat, customer_id,
                 customer_email, is_insecure):
        self.order_serial = order_serial
        self.status = status
        self.date_invoiced = date_invoiced
        self.pricing_name = pricing_name
        self.pricing_surname = pricing_surname
        self.shipping_phone = shipping_phone
        self.shipping_mobile = shipping_mobile
        self.voucher_no = voucher_no
        self.gtjobcode = gtjobcode
        self.courier = courier
        self.gtcount = gtcount
        self.gtflag = gtflag
        self.gen_tax_service = gen_tax_service
        self.payway = payway
        self.admin_comments = admin_comments
        self.courier_comments = courier_comments
        self.customer_comments = customer_comments
        self.pricing_address =pricing_address
        self.pricing_city =pricing_city
        self.pricing_postal =pricing_postal
        self.shipping_address =shipping_address
        self.shipping_city =shipping_city
        self.shipping_postal=shipping_postal
        self.total_vat = total_vat
        self.customer_email = customer_email
        self.customer_id = customer_id
        self.is_insecure = is_insecure

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    comment = db.Column(db.Text,  unique=False, nullable=True)
    action = db.Column(db.String(50), unique=False, nullable=True)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=False)

    def __repr__(self):
        return f"Post('{self.comment}', '{self.date_posted}', '{self.action}')"

    def __init__(self, order_id, action, comment):
        self.comment = comment
        self.action = action
        self.order_id = order_id

class Settings(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    setting = db.Column(db.Text,  unique=False, nullable=True)
    value = db.Column(db.String(100), unique=False, nullable=True)


    def __repr__(self):
        return f"Setting('{self.setting}', '{self.value}')"

    def __init__(self, setting, value):
        self.setting = setting
        self.value = value
