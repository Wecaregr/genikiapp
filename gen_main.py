﻿from flask import Flask, jsonify, request, make_response, render_template, redirect
from sqlalchemy.sql import func
from flask_bootstrap import Bootstrap
from models import db, Order, Post
from datetime import timedelta, datetime, date
from flask_emails import Message, EmailsConfig
import decimal
from genapi import *
from extapi import *
from easysms import *
import json
import sys
if (sys.version_info > (3, 0)):
    # Python 3 code in this block
    print("Python Version: ", sys.version)
else:
     # Python 2 code in this block
    print("Python Version: ", sys.version)
    reload(sys)
    sys.setdefaultencoding('utf-8')

app = Flask(__name__, static_url_path='/static')


app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///genikidb.db'
#app.config['EMAIL_HOST'] = 'smtp.mandrillapp.com'
#app.config['EMAIL_PORT'] = 587
#app.config['EMAIL_TIMEOUT'] = 10
#app.config['EMAIL_HOST_USER']:'wecare.gr'
#app.config['EMAIL_HOST_PASSWORD']:'2z92UCh_hJhq1zKxRhq6zA'

db.init_app(app)
daysfrom=15
daysto = 2
pagination = 50
bootstrap = Bootstrap(app)
progress_download_pages = 0
total_download_pages = 1
wecareapi_uri="http://wecarepharm.gr:5001/api/v1.0"
print('SENDING EMAIL')
email_config = EmailsConfig(config={'EMAIL_HOST':'smtp.mandrillapp.com',
                                      'EMAIL_PORT' : 587,'EMAIL_HOST_USER':'wecare.gr',
                                      'EMAIL_HOST_PASSWORD':'2z92UCh_hJhq1zKxRhq6zA'
                                      })

extapi = Extapi('admin','admin')
ga = genapi('wecare', '2683023055n', 'E94914A8-B34A-4C7B-A7A6-B2A564D0E920')
shoplist = ga.GetShopsList()
print('\n\n|---Extapi token: ', extapi.token)
print('|')
esms = sms('vlasisgeo@gmail.com', '74992')
print('|---SMS API token: ', esms.token)
print('|')

def row2dict(row):
    d = {}
    for column in row.__table__.columns:
        d[column.name] = str(getattr(row, column.name))
    return d


@app.route('/clearacs',methods=['GET'])
def clearacs():
    dbdata = Order.query.filter_by(gtflag=0, gtjobcode='1', is_resolved=0)
    for row in dbdata:
        row.courier = 'ACS'
    db.session.commit()
    dbdata = Order.query.filter_by(gtflag=0, last_checkpoint_date=None, is_resolved=0)
    for row in dbdata:
        row.is_resolved = 1
    db.session.commit()

    return 'ok'

@app.route('/',methods=['POST','GET'])
def get_orders_not_delivered():
    per_page = 10
    # page requests
    page = request.args.get('page', default = 0, type = int)
    order_type = request.args.get('order_type', default = 0)

    print('args get', order_type)
    if request.method == 'POST':
        post_page = request.form.get('post_page',None)
        admin_comment = request.form.get('txtAdminComments')
        is_resolved = request.form.get('is_resolved')

        voucher_no = request.form.get('voucher_no')
        order_id = request.form.get('order_id')
        if is_resolved==None:
            is_resolved=0
        save_admin_comments(order_id, admin_comment, voucher_no)
        change_is_resolved(order_id, is_resolved)
        if post_page!=None:
            page=int(post_page)
        print('|--Getting Not Delivered')
        print('|    |--post_page', post_page)
    print('|    |--page', page)
    rows=[]
    genikionline='OFFLINE'
    #data = com.get_shop_orders_not_delivered(14, 4, page, 20, False)
    if order_type!=0:
        if order_type=='2':
            dbdata = Order.query.filter_by(gtflag=0, is_resolved=2).paginate(page,per_page,error_out=False)
        elif order_type=='0':
            dbdata = Order.query.filter_by(gtflag=0, is_resolved=0, courier='ΓΕΝΙΚΗ').paginate(page,per_page,error_out=False)
        elif order_type=='3':
            dbdata = Order.query.filter_by(gtflag=0, is_resolved=3).order_by("id asc").paginate(page,per_page,error_out=False)
        else:
            dbdata = Order.query.filter_by(gtflag=0, is_resolved=0,last_checkpoint_status=order_type,courier='ΓΕΝΙΚΗ').paginate(page,per_page,error_out=False)

    else:
        dbdata = Order.query.filter_by(gtflag=0, is_resolved=0,courier='ΓΕΝΙΚΗ').paginate(page,per_page,error_out=False)
    try:
        rows = render_checkpoints(dbdata)
        genikionline='ONLINE'
    except  Exception as ex:
        #rows = []
        print(ex)
        genikionline='OFFLINE'
    smsbal='Error SMS!'
    balance_result = esms.balance()
    try:
        smsbal=balance_result['balance']
    except:
        smsbal='Error SMS!'
    data = {'success':True,'count':dbdata.total, 'has_next':dbdata.has_next, 'has_prev':dbdata.has_prev , 'pages':dbdata.pages, 'page':dbdata.page, 'smsbalance':smsbal, 'rows':rows, 'order_type':order_type, 'genikionline':genikionline}
    return render_template('notdelivered.html',data=data)

@app.route('/dumpalltojson', methods=['GET'])
def dumpalltojson():

    dbdata = Order.query.all()
    rows=[]
    if dbdata != None:
        for row in dbdata:
            rows.append(row2dict(row))
    dbdata = Post.query.all()
    print("Order len:",len(rows))
    rposts=[]
    if dbdata != None:
        for row in dbdata:
            rposts.append(row2dict(row))
    print("Post len:",len(rposts))
    data = {"Order":rows, "Post":rposts}
    with open('dbdump.json', 'w') as outfile:
        json.dump(data, outfile)
    return jsonify({"Success":True, "Order":len(rows), "Post":len(rposts)})



@app.route('/cleardelivered', methods=['GET'])
def cleardelivered():
    per_page=20

    dbdata = Order.query.filter_by(gtflag=0, is_resolved=0, courier='ΓΕΝΙΚΗ').paginate(0,per_page,error_out=False)
    for p in range (dbdata.pages,1,-1):
        dbdata = Order.query.filter_by(gtflag=0, is_resolved=0, courier='ΓΕΝΙΚΗ').paginate(p,per_page,error_out=False)
        rows = render_checkpoints(dbdata)

    return redirect('/')

@app.route('/ajax_show_orders', methods=['GET'])
def ajax_show_orders():
    type = request.args.get('type', default = 0, type = int)
    dbdata = Order.query.filter_by(gtflag=0, last_checkpoint_status=type, is_resolved=0, courier='ΓΕΝΙΚΗ').paginate(1,1000,error_out=False)
    rows = render_checkpoints(dbdata)
    data = {'success':True,'rows':rows}
    return render_template('orders_list.html',data=data)


@app.route('/ajax_send_sms', methods=['POST'])
def ajax_send_sms():
    id = request.form['id']
    smstext = request.form['smstext']
    mobile = request.form['mobile']
    print('|--AJAX')
    print('|     |--sendsms', id, mobile, smstext)
    data='Error Sending SMS'
    if mobile!=None and smstext!=None:
        print('sending sms', mobile, smstext)
        if sendsms(smstext, mobile, id):
            data='SMS OK'
        else:
            data='Error Sending SMS'
            print('|--AJAX send sms error')
    return jsonify({'result':'success', 'responce':data})

@app.route('/ajax_tel_call', methods=['POST'])
def ajax_tel_call():
    id = request.form['id']
    calltext = request.form['calltext']

    print('|--AJAX')
    print('|     |--sendsms', id,calltext)
    data='Tel OK'
    if calltext!=None:
        record_telephone_call(id, calltext)
        print('Tel call', calltext)

    return jsonify({'result':'success', 'responce':data})


@app.route('/ajax_updaterow', methods=['POST'])
def ajax_updaterow():
    id = request.form['id']

    print('|--AJAX')
    order = Order.query.filter_by(id=id).first()
    row = order_render(order)
    return render_template('action_section.html',ord=row)

@app.route('/ajax_updateposts', methods=['POST'])
def ajax_updateposts():
    id = request.form['id']
    posts = {'success':True, 'rows':[]}
    print('|--AJAX')
    postsdb = Post.query.filter_by(order_id=id)
    for p in postsdb:
        post_row = row2dict(p)
        posts['rows'].append(post_row)
    return render_template('posts_section.html',posts=posts)

@app.route('/ajax_send_email', methods=['POST'])
def ajax_send_email():
    id = request.form['id']
    emailtext = request.form['emailText']
    subject = request.form['subject']
    recipient_email = request.form['recipient_email']
    recipient_name = request.form['recipient_name']
    action = 'email'
    if recipient_email == 'emailGeniki':
        recipient_email='flp@taxydromiki.gr'
        action='ΓΕΝΙΚΗ'
        recipient_name = 'Γενική Φιλιππιάδας'
    print('|--AJAX send email')
    mail_result = send_email(subject, emailtext, recipient_name, recipient_email, id, action) #change to recipient_email when ready
    #copy_mail_result = send_email(subject, emailtext, recipient_name, recipient_email, id, action) #change to recipient_email when ready
    data=''
    if mail_result:
        data='Email OK'
    else:
        data='Error'
        print('|--AJAX send email error')
    return jsonify({'result':'success', 'responce':data})


@app.route('/settings')
def settings():
    return render_template('settings.html',data=None)

@app.route('/order_posts', methods=['POST', 'GET'])
def order_posts():
    id = request.args.get('id', default = 0, type = int)
    page = request.args.get('page', default = 0, type = int)
    row = Order.query.filter_by(id=id).first()
    ord = order_render(row)
    update_get_shop_orders([ord['order_serial']])
    if request.method == 'POST':

        print('getting post data')
        try:
            smsmobile =  request.form['themobile']
            smstext = request.form['smstext']
        except:
            smsmobile =  None
            smstext = None
        try:
            telephonecall = request.form['telephonecall']
        except:
            telephonecall=None
        if smsmobile!=None or smstext!=None:
            print('sending sms', smsmobile, smstext)
            sendsms(smstext, smsmobile, id)
            save_admin_comments(ord['order_serial'], '{0} --> SMS SEND\n{1}'.format(datetime.utcnow(),ord['admin_comments']), ord['voucher_no'])
        if telephonecall!=None:
            print('Telephone call')
            record_telephone_call(ord['id'], telephonecall)
            save_admin_comments(ord['order_serial'], '{0} --> Telephone Call\n{1}'.format(datetime.utcnow(),ord['admin_comments']), ord['voucher_no'])

    ord['smsphone']=esms.mobilecheck(ord['shipping_phone'])
    ord['smsmobile']=esms.mobilecheck(ord['shipping_mobile'])
    print(ord['smsphone'], ord['smsmobile'])
    smstext=''
    try:
        smstext=''
        if ord['lastCheck']==None :
            smstext ='H παραγγελία σας με voucher {0}'.format(ord['voucher_no'])
        else:
            smstext ='H παραγγελία σας με voucher {0} βρίσκεται στη Γενική Ταχ. {1} τηλ {2}. Σήμανση: {3}'.format(ord['voucher_no'], ord['lastCheck']['shop_details']['City'], ord['lastCheck']['shop_details']['Telephone'],ord['lastCheck']['Status'])

        ord['smstext']=smstext
    except:
        smstext=''
    posts = {'success':True,'order':ord, 'rows':[], 'page':page}
    postsdb = Post.query.filter_by(order_id=row.id)
    for p in postsdb:
        post_row = row2dict(p)
        posts['rows'].append(post_row)
    return render_template('orderposts.html', posts=posts)


@app.route('/get_orders_from_wecare')
def get_orders_from_wecare():
    progress_download_pages = 0
    total_download_pages = 1
    result = extapi.callExtApi({'service':'get_shop_orders_not_delivered', 'daysfrom':daysfrom,
                                'daysto':daysto, 'page':1, 'pagination':pagination})
    print('|--Total count orders {1}. {0} Pages'.format(result['pages'], result['count']))
    total_download_pages = result['pages']
    for i in range(1, result['pages']+1):
        result = extapi.callExtApi({'service':'get_shop_orders_not_delivered', 'daysfrom':daysfrom,
                                    'daysto':daysto, 'page':i, 'pagination':pagination})
        print('|    |--{0} from {1}'.format(result['page'], result['pages']))
        insert_update(result['rows'])
        progress_download_pages=i
    return render_template('settings.html',data=result)

@app.route('/download_data')
def download_data():
    smsbal='Error SMS!'
    balance_result = esms.balance()
    try:
        smsbal=balance_result['balance']
    except:
        smsbal='Error SMS!'
    data = {'success':True,'smsbalance':smsbal,'genikionline':'ONLINE'}
    return render_template('get_orders_from_wecare.html', data = data)

@app.route('/ajax_polling', methods=['GET'])
def ajax_polling():
    result=None
    result={'success':True}
    result['progress_download_pages']=progress_download_pages
    result['total_download_pages']=total_download_pages
    result['percent']=round(100*progress_download_pages/total_download_pages, 0)
    return render_template('progress_bar.html',progress_data=result)

@app.route('/ajax_get_orders_from_wecare', methods=['GET'])
def ajax_get_orders_from_wecare():
    global progress_download_pages
    global total_download_pages
    progress_download_pages = 0
    total_download_pages = 1
    totalrows=0
    result = extapi.callExtApi({'service':'get_shop_orders_not_delivered', 'daysfrom':daysfrom,
                                'daysto':daysto, 'page':1, 'pagination':pagination})
    print('|--Total count orders {1}. {0} Pages'.format(result['pages'], result['count']))
    total_download_pages = result['pages']
    for i in range(1, result['pages']+1):
        result = extapi.callExtApi({'service':'get_shop_orders_not_delivered', 'daysfrom':daysfrom,
                                    'daysto':daysto, 'page':i, 'pagination':pagination})
        print('|    |--{0} from {1}'.format(result['page'], result['pages']))
        insert_update(result['rows'])
        progress_download_pages=i
        totalrows+=1
    result['download_status']={'status':'success', 'totalrows':totalrows, 'pages':progress_download_pages}
    return render_template('progress_bar.html',data=result)

def save_admin_comments(order_id, admin_comment, voucher_no):
    # Save admin comments to wecare
    print('|    |--',admin_comment, order_id)
    if  order_id!=None and order_id!='':
        admin_comment_result = extapi.callExtApi({'service':'updateorder_admin_comments','order_id':order_id, 'admin_comment':admin_comment, 'gtcode':voucher_no})
        update_get_shop_orders([order_id])
        print('|   |--admin comment change',admin_comment_result)


def render_checkpoints(dbdata):
    rows=[]
    if dbdata != None:
        for row in dbdata.items:
            rows.append(order_render(row))
    return rows

def order_render(row):
    # Geniki api initialize

    ord= row2dict(row)
    ord['actions']=[]
    postsdb = Post.query.filter_by(order_id=row.id)
    for p in postsdb:
        post_row = row2dict(p)
        ord['actions'].append(post_row['action'])

    track = None
    track = ga.TrackAndTrace(ord['voucher_no'])
    chpoint=None
    if track['Checkpoints']!=None:
        for chp in track['Checkpoints']['Checkpoint']:
            if chp['Latitude']!=0:
                chp['gps']='https://www.google.com/maps/@{0},{1},19z'.format((chp['Latitude']/100000),(chp['Longitude']/100000))
            else:
                chp['gps']=None
            if chp['Status']=='Αδυναμία παράδοσης - Άγνωστος Παραλήπτης':
                chp['alert']='danger'
            elif chp['Status']=='Αδυναμία παράδοσης - Παραμονή με συνεννόηση'  or chp['Status']=='Αδυναμία παράδοσης - Δεν Διακινήθηκε' or chp['Status']=='Αδυναμία παράδοσης - Παράδοση σε 2-3 ημέρες':
                chp['alert']='warning'
            elif chp['Status']=='Αδυναμία παράδοσης - Απών Σημείωμα':
                chp['alert']='secondary'
            elif chp['Status']=='Χρέωση σε κούριερ':
                chp['alert']='light'
            elif chp['Status']=='Καταχώρηση στοιχείων παράδοσης' or chp['Status']=='Παράδοση αποστολής':
                chp['alert']='success'
                change_is_resolved(ord['order_serial'], 1)
            elif chp['Status']=='Επιστροφή στον αρχικό αποστολέα':
                chp['alert']='success'
                change_is_resolved(ord['order_serial'], 2)
            else:
                chp['alert']='info'

        chpoint = chp
    else:
        #change_is_resolved(ord['order_serial'], 4)
        print('No checkpoints ')
    if chpoint!=None:
        try:
            shop = next(item for item in shoplist['Shops']['Shop'] if item["City"] == chpoint['Shop'] )
            chpoint['shop_details']=shop
        except StopIteration:
            pass
    ord['track'] = track
    ord['lastCheck'] = chpoint
    #Check if there is a new status afte last post
    lastpost = postsdb.order_by(Post.id.desc()).first()
    ord['hasnewstatus']=False
    if lastpost!=None:
        #2019-05-06 11:15:20
        #last_scan_datetime = datetime.strptime('Jun 1 2005  1:33PM', '%b %d %Y %I:%M%p')
        if ord['lastCheck']['StatusDate']>lastpost.date_posted:
            print('NEW CHECKPOINT',ord['lastCheck']['StatusDate'],lastpost.date_posted)
            ord['hasnewstatus']=True
            print(ord)

    update_last_check_point(ord['id'], ord)
    return ord

def update_get_shop_orders(orderids):
    try:
        result = extapi.callExtApi({'service':'get_shop_orders', 'order_ids':orderids})
        print('|--update get shop order id',orderids)
        insert_update(result['rows'])
        return True
    except:
        return False
def update_last_check_point(id, ord):
    chpoint=ord['lastCheck']
    if chpoint!=None:
        dbordcheck = Order.query.filter_by(id=id).first()
        dbordcheck.last_checkpoint_city = chpoint['Shop']
        dbordcheck.last_checkpoint_status = chpoint['Status']
        dbordcheck.last_checkpoint_date = chpoint['StatusDate']
        db.session.commit()
        print('|--Lastcheckpoint save', id, dbordcheck.order_serial, dbordcheck.last_checkpoint_status, dbordcheck.last_checkpoint_date, 'isresoved: ',dbordcheck.is_resolved)

def change_is_resolved(order_id, is_resolved):
    if order_id!=None and order_id!='':
        if is_resolved==None:
            is_resolved=0
        dbordcheck = Order.query.filter_by(order_serial=order_id).first()
        if dbordcheck.is_resolved==2 and is_resolved==1:
            dbordcheck.is_resolved = 3
        else:
            dbordcheck.is_resolved = is_resolved
        dbordcheck.is_resolved_timestamp = datetime.utcnow()
        db.session.commit()
        print('|   |--is_resolved_changed',is_resolved)

################################################################################
# Actions
################################################################################
def insert_post(order_id, action, comment):
    print('|--New Post',order_id, action, comment)
    newpost = Post(order_id, action, comment)
    db.session.add(newpost)
    db.session.commit()

def insert_comment(order, comment):
    insert_post(order, 'COM', '{0}'.format(comment))

def record_telephone_call(order, telephonecall):
    insert_post(order, 'TEL', '[{0}] '.format(telephonecall))

def sendsms(smstext, mobile, order):
    result = esms.sendsms(smstext,'wecare.gr',mobile)
    insert_post(order, 'SMS', '[{1}] sms: {0}'.format(smstext, result['remarks']))
    return True

def send_email(subject,html,recipient_name, recipient_email, id, action):
    message = Message(config=email_config, html=html, subject=subject,
            mail_from=("wecare.gr", "info@wecare.gr"))

    r = message.send(to=(recipient_name, recipient_email))

    if r.status_code not in [250, ]:
        #message is not sent, deal with this
        return False
    else:
        insert_post(id, action, '[{0}] {1}'.format(recipient_email, subject))
        #r = message.send(to=('wecare', 'info@wecare.gr'))
        return True


################################################################################
# Update db from Wecare API
################################################################################
def get_customer_email_insecure(customer_id):
    try:
        result = extapi.callExtApi({'service':'get_by_id', 'object':'CUSTOMERS', 'key':customer_id})
        print('|--Get customer id',customer_id, result['result']['mail'])
        return {'email':result['result']['mail'], 'is_insecure':result['result']['is_insecure']}
    except:
        return None, None

def insert_update(rows):
    for ord in rows:
        dbordcheck = Order.query.filter_by(order_serial=ord['order_serial']).first()
        if dbordcheck==None:
            print('|--New order', ord['order_serial'])
            #ord['shipping_phone']=esms.mobilecheck(ord['shipping_phone'])
            #ord['shipping_mobile']=esms.mobilecheck(ord['shipping_mobile'])
            courier  = 'ΓΕΝΙΚΗ'
            if ord['gtjobcode']=="1":
                courier  = 'ACS'
            result = get_customer_email_insecure(ord['customer_id'])
            neworder = Order(ord['order_serial'], ord['status'], datetime.strptime(ord['invoised_date_time'], "%a, %d %b %Y %H:%M:%S GMT").date(),
                             ord['pricing_name'],ord['pricing_surname'], ord['shipping_phone'],
                             ord['shipping_mobile'],ord['gtcode'], ord['gtjobcode'],courier,
                             ord['gen_tax_service'], ord['gtflag'], ord['gtcount'], ord['payway'],
                             ord['courier_comments'], ord['customer_comments'], ord['admin_comments'],
                             ord['pricing_address'], ord['pricing_city'], ord['pricing_postal'],
                             ord['shipping_address'], ord['shipping_city'], ord['shipping_postal'],
                             ord['total_vat'], ord['customer_id'], result['email'], result['is_insecure'])

            db.session.add(neworder)
            db.session.commit()
            print("|--order inserted in db ", ord['order_serial'])
        else:
            courier  = 'ΓΕΝΙΚΗ'
            if ord['gtjobcode']=='1':
                courier  = 'ACS'
            print("|--order exists in db ", dbordcheck.order_serial)
            #print(dbordcheck.customer_email)
            #dbordcheck.is_resolved=0
            dbordcheck.date_last_download = datetime.utcnow()
            dbordcheck.status = ord['status']
            dbordcheck.gtcount = ord['gtcount']
            dbordcheck.gtflag = ord['gtflag']
            dbordcheck.voucher_no = ord['gtcode']
            dbordcheck.gtjobcode = ord['gtjobcode']
            dbordcheck.voucher_no = ord['gtcode']
            dbordcheck.admin_comments = ord['admin_comments']
            dbordcheck.courier = courier
            #dbordcheck.shipping_phone = esms.mobilecheck(ord['shipping_phone'])
            #dbordcheck.shipping_mobile = esms.mobilecheck(ord['shipping_mobile'])
            if ord['gtflag']==1:
                dbordcheck.is_resolved=1
                dbordcheck.is_resolved_timestamp = datetime.utcnow()
            db.session.commit()


if __name__ == '__main__':
    app.run(
        host="0.0.0.0",
        port=int("5006"),
        debug=True,
        threaded=True

    )
